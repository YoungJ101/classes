#include <iostream>
#include <string>
#include "Demo.h"

using namespace std;



OnlineVid::OnlineVid()
{
	views = 0;
	id = "";
}

OnlineVid::OnlineVid(unsigned val, string identity)
{
	views = val;
	id = identity;
}

 void OnlineVid::setViews(unsigned val )
 {
   views = val;
 }

 unsigned OnlineVid::getViews()
 {
  return views;
 }
 
 void OnlineVid::setId(string identity)
 {
	 id = identity;
 }
 
 string OnlineVid::getId()
 {
	 return id;
 }
 
 int main ()
 {
	 return 0;
 }
