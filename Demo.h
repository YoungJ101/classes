#include <string>
class OnlineVid
 {
    private:  //properties
         unsigned  views;
         std::string id; 
         


    public:  //methods
        
         OnlineVid();//default constructor
         OnlineVid(unsigned, std::string); 

          void setViews(unsigned);
          unsigned getViews();
          void setId(std::string);
          std::string getId();

 };

